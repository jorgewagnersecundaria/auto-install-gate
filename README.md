# Curso Gate

## Autor: Jorge Wagner Esteves da Silva

## ---------------------------------------------------------------------------------------------------------------------------------

Esse repositório se destina aos arquivos do minicurso de Gate - Monte Carlo do Programa de Residência em Física Médica do Instituto 
Nacional do Câncer. 

Os scripts aqui presentes se destinan a facilitar o processo de instalação do Gate e de suas dependências. 

O processo de instalação foi testado no Ubuntu 20.04 com desktop XFCE4. 

Todos os arquivos necessários se encontram no repositório.

Para clonar o repositório, você deverá:

1) Instalar o Ubuntu 20.04:

    - Fazer o download a partir de https://ubuntu.com/download/desktop/thank-you?version=20.04.1&architecture=amd64
    - Criar uma mídia de instalação, diversos tutoriais estão disponíveis na Internet de como criar a mídia necessária;
    - Proceder com a instalação do Ubuntu.

2) Atualizar o Ubuntu 20.04:

    - Abrir uma janela do terminal.
    - Executar o comando: sudo apt update && sudo apt upgrade -y

    Obs.: O comando sudo vai demandar a entrada da senha do usuário.

3) Instalar o aplicativo do git: 

    - Executar os comandos:
    
         sudo apt install git git-lfs
             
    - Após a instalação, verificar a versão do git com o comando:  
    
        git --version  

4) Clonar o repositório com o comando:  
    
    git clone https://jorgewagnersecundaria@bitbucket.org/jorgewagnersecundaria/install-gate.git

    - Ao executar o script, todos os programas necesários para a instalação do Gate e algumas ferramentas adicionais
      serão baixadas e instaladas.

5) De forma a executar o script, você deve abrir o terminal, navegar ate o diretório clonado e utilizar o comando:
   
       cd caminho-para-o-diretório, onde caminho-para-o-diretório consiste em um endereço do tipo /algo/algomais
       chmod +x auto-install-gate.sh

    - Verificar que não existe um diretório software-gate em /home/user e executar o comando:
    
        ./auto-install-gate.sh
 
6) Leia atentamente as informações apresentadas no terminal antes de decidir seguir com a instalação.

7) Um arquivo README.md será criado na pasta /home/user/software-gate e nele estarão as informações que foram apresentadas no
   início do processo de instalação. Elas dizem respeito aos programas instalados e suas respectivas licenças de uso.

