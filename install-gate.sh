#!/bin/bash

################################
# Faz para a captura de erro
# Encerra a execução do script
################################
set -e

MYPATH=$(pwd)
sGATE="${HOME}/software-gate"
MYLOGPATH="${sGATE}/log" 
MYLOG="${MYLOGPATH}/install-gate.log"
MYBRC="${HOME}/.bashrc"
MYDISCLAIMER="${sGATE}/README.md"

############################
# Captura de erros
trap 'catch $?' EXIT

catch() {
  if [ "$1" != "0" ]; then
     printf "\n*****************************************************"      | tee -a ${MYLOG}
     printf "\n   Ocorreu um erro: $1                               "      | tee -a ${MYLOG}
     printf "\n   Execução do script de instalação do Gate falhou.  "      | tee -a ${MYLOG}
     printf "\n*****************************************************\n\n"  | tee -a ${MYLOG}
  fi
  exit 1
}


#################################################
# Executa a limpeza do terminal
clear

#################################################
# Ajusta o terminal para 30 linhas e 160 colunas
printf '\e[8;30;160t'

################################################
# Criando os diretórios necessários
mkdir ${sGATE}
mkdir ${MYLOGPATH}
cd ${sGATE}

################################################
# Iniciando o registro de instalação

printf "\n==================================="       | tee -a ${MYLOG}
printf "\n==                               =="       | tee -a ${MYLOG}
printf "\n== Iniciando log de instalação:  =="       | tee -a ${MYLOG}
printf "\n==                               =="       | tee -a ${MYLOG}
printf "\n===================================\n\n"   | tee -a ${MYLOG}

printf "\nCriado o diretório software-gate.\n\n"  | tee -a ${MYLOG}

printf "\n####################################################################################"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Script para a Instalação do Gate v9.0 e do Geant4 em Sistemas Ubuntu 20.04  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Criador: Jorge Wagner Esteves da Silva, DSc.                                ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Objetivo: Facilitar a instalação dos softwares Geant4 e Gate e de           ###"      | tee -a ${MYDISCLAIMER}
printf "\n###            suas dependências, para os alunos do mini-curso de GATE.          ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Script criado em 28 de agosto de 2020                                       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  --------------------------------------------------------------------------- ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Importante:                                                                 ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Esse não é um método oficial de instalação do Geant4 ou do GATE, nem se     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  propõe a ser.                                                               ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  O Geant4 é oficialmente distribuído através da página:                      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  https://geant4.web.cern.ch/                                                 ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  O GATE é oficialmente distribuído através da página:                        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  http://www.opengatecollaboration.org/                                       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Maiores informações quanto ao processo de instalação e das dependências de  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  cada um dos softwares podem ser obtidas nas respectivas páginas.            ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Serão instalados ou atualizados, com o uso do gerenciador de pacotes        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  apt-get, os softwares:                                                      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  zip, unzip, gcc, g++, libssl-dev, wget, cmake-curses-gui, libxaw7-dev,      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libxaw7, libclang-dev, gperf, libxmu-dev, libxmu-headers, expat,            ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libmotif-common, freeglut3, freeglut3-dev, mesa-utils, mesa-common-dev,     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libglu1-mesa-dev, qt5-default, libqt5webkit5-dev, qttools5-dev,             ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  qttools5-dev-tools, libqt5x11extras5, libqt5x11extras5-dev, libqt5xdg-dev,  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libqt5xdg3, python3 python3-pip, dpkg-dev, cmake binutils, libx11-dev,      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libxpm-dev, libxft-dev, libxext-dev, libcurl4-openssl-dev, gfortran,        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libpcre3-dev, xlibmesa-glu-dev, libglew1.5-dev, libftgl-dev,                ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libmysqlclient-dev, libfftw3-dev, libcfitsio-dev, graphviz-dev,             ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libavahi-compat-libdnssd-dev, libldap2-dev python-dev, libxml2-dev,         ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libkrb5-dev, libgsl0-dev, git, git-lfs, libavcodec-dev, libavformat-dev,    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libavutil-dev, libboost-dev, libdouble-conversion-dev, libeigen3-dev,       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libexpat1-dev, libfontconfig-dev, libfreetype6-dev, libgdal-dev,            ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libglew-dev, libhdf5-dev, libjpeg-dev, libjsoncpp-dev, liblz4-dev,          ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  liblzma-dev, libnetcdf-dev, libnetcdf-cxx-legacy-dev, libogg-dev,           ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libpng-dev, libpython3-dev, libqt5opengl5-dev, libqt5x11extras5-dev,        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libsqlite3-dev, libswscale-dev, libtheora-dev, libtiff-dev, libxml2-dev,    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libxt-dev, qtbase5-dev, qttools5-dev, zlib1g-dev, libpulse-dev, libnss3,    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  libglu1-mesa, apt-transport-https, curl, Visual Studio Code                 ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Serão instalados ou atualizados, com o uso do gerenciador de pacotes pip3   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  os seguintes módulos do python3 e outros softwares:                         ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  numpy, matplotlib, scipy, pydicom, pandas, uproot, SimpleITK, jupyter       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  notebook, jupyterlab, voila                                                 ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Serão ainda instalados outros softwares, maiores informações a respeito     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  dos mesmos e de suas licenças de uso podem ser obtidas nas respectivas      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  páginas:                                                                    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  1) CMAKE v3.18.1: https://cmake.org/                                        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  2) Xerces-C++ v3.2.3: https://xerces.apache.org/xerces-c/                   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  3) CLHEP v2.4.1.3: http://proj-clhep.web.cern.ch/proj-clhep/                ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  4) Root v6.22.00: http://proj-clhep.web.cern.ch/proj-clhep/                 ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  5) VTK 7.1.1: https://vtk.org/                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  6) ITK e RTK v5.1.0: https://itk.org/ e https://www.kitware.eu/platforms/   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  7) 3D Slicer v4.10.2: https://www.slicer.org/                               ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  8) vV v1.4: https://www.creatis.insa-lyon.fr/rio/vv                         ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Ainda, com o intuito de facilitar o processo de instalação dos softwares a  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  serem utilizados no curso, será instalada uma versão do Fiji, com os        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  plugins abaixo listados, já configurados. Informações com relação ao uso    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  e licenças do referido software e plugin podem ser obtidas em:              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  a) Fiji: https://fiji.sc/                                                   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  b) Beth Israel plugin para o Fiji - ImageJ : http://petctviewer.org/        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  c) IAEA NMQC plugin para o Fiji - ImageJ: https://humanhealth.iaea.org/HHW/ ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  MedicalPhysics/NuclearMedicine/QualityAssurance/NMQC-Plugins/index.html     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  d) TPCoolection plugin para o ImageJ, originalmente disponível em:          ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  http://www.med.harvard.edu/JPNM/ij/plugins/TPcollection.html                ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Observação: Aparentemente o endereço onde o TPCollection plugin se          ###"      | tee -a ${MYDISCLAIMER}
printf "\n###              encontrava disponível não se encontra mais ativo.               ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Uma cópia da licença do Fiji se encontra disponível no diretório            ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  software-gate, criado durante o processo de instalação dos softwares por    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  esse script.                                                                ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Será ainda instalado o libtorch v1.4.0-cpu, cujo código fonte foi retirado  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  do vGate 9.0, uma vez que diversas tentativas de compilar o Gate v9.0 com   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  as versões mais atuais dessa biblioteca se mostraram infrutíferas,          ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  aoresentando erros. Foi realizada uma consulta a users mailing list do      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Gate. Entretanto, até omomento, nenhuma resposta foi obtida com relação     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  aos erros gerados com o uso de versões mais recentes dessa biblioteca.      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Maiores informações com relação à biblioteca libtorch e sua licença pode    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  ser obtida em:                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  https://github.com/pytorch/pytorch/blob/master/LICENSE                      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Uma cópia da mesma se encontra disponível no diretório software-gate  ,     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  criado durante o processo de instalação dos softwares por esse script.      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Serão ainda clonados os repositórios:                                       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  GateContrib: https://github.com/OpenGATE/GateContrib.git                    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  gate-exercices: https://github.com/dsarrut/gate-exercices.git               ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  Por automatizar o processo de instalação, em muitos casos não serão         ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  apresentadas as declarações de propriedade dos softwares de terceiros, mas  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  você deve estar ciente que, ao utilizar esse script, você está concordando  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  com cada uma das condições de uso estabelecidas pelos proprietários dos     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  mesmos e suas condições de licenciamento.                                   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  O autor do script não se reponsabiliza pela sua aceitação de tais           ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  condições. Você é fortemente encorajado a fazer o processo de instalação    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  manualmente e a observar cuidadosamente as condições de fornecimento e uso  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  de cada um dos softwares instalados por esse script.                        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###  --------------------------------------------------------------------------- ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Lembre-se que o processo de instalação irá provocar alterações no seu      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   sistema. É de sua inteira responsabilidade garantir que os seus arquivos   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   tenham sido armarzenados (backup) de forma a que possam ser restaurados    ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   em caso de algum problema. O autor do script não se responsabiliza por     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   problemas que possam advir da utilização desse script, incluindo a perda   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   de dados.                                                                  ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       ***************************************************************        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       *                                                             *        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       *    Você tem a opção de instalar o Gate no modo de máquina   *        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       *    virtual, que rodará no Virtual Box, o mesmo pode ser     *        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       *    obtido em http://www.opengatecollaboration.org/node/93   *        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       *                                                             *        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###       ***************************************************************        ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Cabe a você decidir se deve ou não prosseguir com a instalação do Gate     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   v9.0 através desse script. Esse script foi testado no sistema              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   operacional Ubuntu 20.04. Entretanto podem ocorrer erros durante o         ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   processo de instalação em sua máquina.                                     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Durante alguns passos, você poderá ser questionado se deseja ou            ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   não prosseguir.                                                            ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Caso aconteçam erros, o script não prevê qualquer tipo de reversão da      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   instalação. Entretanto, a maior parte dos softwares serão instalados       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   no diretório ~/software-gate.                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Contudo, não existe um controle sobre o local de instalação das várias     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   dependências.                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Dessa forma é recomendado que você leia o script antes e verifique se      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   existe incompatibilidade com outros softwares que você utiliza em seu      ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   sistema.                                                                   ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   Registros das diversas operações de instalação serão gerados na pasta:     ###"      | tee -a ${MYDISCLAIMER}
printf "\n###   ~/software-gate/log.                                                       ###"      | tee -a ${MYDISCLAIMER}
printf "\n###                                                                              ###"      | tee -a ${MYDISCLAIMER}
printf "\n####################################################################################\n\n"  | tee -a ${MYDISCLAIMER}

###################################
# Verificar se o usuário deseja
# continuar com a Instalação do
# Gate v9.0
###################################

while true; do
  read -r -p $'\e[41mVocê leu as informações apresentadas acima? Deseja prosseguir com a instalação do Gate v9.0 utilizando esse script (S|N)?\e[0m ' input
  case ${input} in
      [Ss])
         printf "\n\nVocê leu as informações apresentadas acima? Deseja prosseguir com a instalação do Gate V9.0 utilizando esse script (S|N)? ${input}.\n" | tee -a ${MYLOG}
         printf "\nContinuando com a instalação do Gate v9.0...\n" | tee -a ${MYLOG}
         break;;
      [Nn])
         clear
         printf "\n\nVocê leu as informações apresentadas acima? Deseja prosseguir com a instalação do Gate V9.0 utilizando esse script (S|N)? ${input}.\n" >> ${MYLOG}
         printf "\nVocê optou por não seguir com o processo de instalação do Gate v9.0.\n" | tee -a ${MYLOG}
         printf "\nRemovendo o diretório ${sGATE}"
         printf "\nCopiando o log de instalação para o diretório ${MYPATH}\n"
         printf "\nEncerrando a instalação do Gate v9.0.\n" | tee -a ${MYLOG}
         printf "\nEncerrando o log.\n" | tee -a ${MYLOG}
         printf "\n###################################\n\n"   | tee -a ${MYLOG}

         cp ${MYLOG} ${MYPATH} 
         rm -r ${sGATE}
         exit;;
      *)
         printf "\n\nPor favor responda S para sim ou N para não.\n\n" | tee -a ${MYLOG};;
  esac
done

printf "\n---------------------------------------------------" | tee -a ${MYLOG}
printf "\n--                                               --" | tee -a ${MYLOG}
printf "\n-- Iniciando a instalação do Geant4 v10.06.p02:  --" | tee -a ${MYLOG}
printf "\n--                                               --" | tee -a ${MYLOG}
printf "\n---------------------------------------------------"   | tee -a ${MYLOG}

printf "\nAtualizando o sistema:\n\n" | tee -a ${MYLOG}
       sudo apt-get update | tee -a ${MYLOG}

printf "\n\n"
       sudo apt-get -y upgrade | tee -a $MYLOG

printf "\n\nSistema atualizado.\n\n" | tee -a ${MYLOG}

printf "\n\nInstalando dependências e alguns softwares de suporte:\n" | tee -a ${MYLOG}

printf "\nInstalando ferramentas zip e unzip:\n\n" | tee -a ${MYLOG}
       sudo apt-get install -y zip unzip  | tee -a ${MYLOG}

printf "\n\nInstalando ferramentas de compilação gcc e g++:\n\n" | tee -a ${MYLOG}
       sudo apt-get install -y build-essential gcc g++  | tee -a ${MYLOG}

printf "\n\nInstalando libssl-dev:\n\n"  | tee -a ${MYLOG}
      sudo apt-get install -y libssl-dev | tee -a ${MYLOG}

printf "\n\nInstalando wget:\n\n"  | tee -a ${MYLOG}
      sudo apt-get install -y wget | tee -a ${MYLOG}

printf "\n\nIniciando a instalação do CMAKE v3.18.1:"  | tee -a ${MYLOG}

printf "\n\nBaixando o CMAKE v3.18.1 de https://github.com/Kitware/CMake/releases/download/v3.18.1/cmake-3.18.1.tar.gz:\n\n"  | tee -a ${MYLOG}
       wget https://github.com/Kitware/CMake/releases/download/v3.18.1/cmake-3.18.1.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nDescompactando o CMAKE: v3.18.1:\n\n"  | tee -a ${MYLOG}
       tar xfv cmake-3.18.1.tar.gz 2>&1 | tee -a ${MYLOG}
       cd ${sGATE}/cmake-3.18.1

printf "\n\nExecutando o arquivo de configuração do CMAKE v3.18.1. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       ./configure 2>&1 | tee -a ${MYLOGPATH}/001-cmake-config.log

printf "\n\nCompilando o CMAKE v3.18.1. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       make 2>&1 | tee -a ${MYLOGPATH}/002-cmake-build.log

printf "\n\nInstalando o CMAKE v3.18.1. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       sudo make install  2>&1 | tee -a ${MYLOGPATH}/003-cmake-install.log

printf "\n\nInstalando o cmake-curses-gui:\n\n" | tee -a ${MYLOG}
      sudo apt-get -y install cmake-curses-gui  | tee -a ${MYLOG}

printf "\n\nInstalando as bibliotecas X11, Xmu, libclang, expat, libmotif:\n\n" | tee -a ${MYLOG}
       sudo apt-get -y install libxaw7-dev libxaw7 libclang-dev gperf libxmu-dev libxmu-headers expat libmotif-common | tee -a ${MYLOG}

printf "\n\n Instalando o OpenGL:\n\n" | tee -a ${MYLOG}
       sudo apt-get -y install freeglut3 freeglut3-dev mesa-utils | tee -a ${MYLOG}

printf "\n\nInstalando as bibliotecas MesaGL:\n\n" | tee -a ${MYLOG}
      sudo apt-get -y install mesa-common-dev libglu1-mesa-dev | tee -a ${MYLOG}

printf "\n\nInstalando o Qt5:\n\n" | tee -a ${MYLOG}
      sudo apt-get -y install qt5-default libqt5webkit5-dev qttools5-dev qttools5-dev-tools libqt5x11extras5 libqt5x11extras5-dev libqt5xdg-dev libqt5xdg3 | tee -a ${MYLOG}

printf "\n\nInstalando o curl:\n\n" | tee -a ${MYLOG}
      sudo apt-get -y install curl  | tee -a ${MYLOG}

printf "\n\nIniciando a instalação do Xerces-C++ v3.2.3:\n\n" | tee -a ${MYLOG}
      cd ${sGATE}

printf "\n\nBaixando o Xerces-C++ v3.2.3:\n\n"| tee -a ${MYLOG}
       wget https://downloads.apache.org/xerces/c/3/sources/xerces-c-3.2.3.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nDescompactando o Xerces-C++ v3.2.3:\n\n" | tee -a ${MYLOG}
       tar xfv xerces-c-3.2.3.tar.gz 2>&1 | tee -a ${MYLOG}
       cd ${sGATE}/xerces-c-3.2.3

printf "\n\nIniciando a configuração para a compilação do Xerces-C++ v3.2.3. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       ./configure 2>&1 | tee -a ${MYLOGPATH}/004-Xerces-C-configure.log

printf "\n\nCompilando o Xerces-C++ v3.2.3. Um log de compilação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
        make 2>&1 | tee -a ${MYLOGPATH}/005-Xerces-C--build.log

printf "\n\nInstalando o Xerces-C++ v3.2.3. Um log de compilação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       sudo make install  2>&1 | tee -a ${MYLOGPATH}/006-Xerces-C-install.log

printf "\n\Inciando a instalação do CLHEP v2.4.1.3:\n\n" | tee -a ${MYLOG}

printf "\n\nCriando o diretório CLHEP em ${sGATE}\n\n:" | tee -a ${MYLOG}
       cd ${sGATE}
       mkdir CLHEP
       cd ${sGATE}/CLHEP

printf "\n\nBaixando o CLHEP v2.4.1.3:\n\n" | tee -a ${MYLOG}
       wget http://proj-clhep.web.cern.ch/proj-clhep/dist1/clhep-2.4.1.3.tgz  2>&1 | tee -a ${MYLOG}

printf "\n\nDescompactando o CLHEP v2.4.1.3:\n\n" | tee -a ${MYLOG}
       tar -zxvf clhep-2.4.1.3.tgz 2>&1 | tee -a ${MYLOG}

printf "\n\nCriando o diretório para a compilação do CLHEP v2.4.1.3:\n\n" | tee -a ${MYLOG}
       mkdir ${sGATE}/CLHEP/CLHEP-build
       cd ${sGATE}/CLHEP/CLHEP-build

printf "\n\nIniciando a configuração para a compilação do CLHEP v2.4.1.3. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       cmake -DCMAKE_INSTALL_PREFIX=$sGATE/CLHEP/CLHEP-install \
       ${sGATE}/CLHEP/2.4.1.3/CLHEP  2>&1 | tee -a ${MYLOGPATH}/007-CLHEP-config.log

printf "\n\Iniciando a compilação do CLHEP v2.4.1.3, serão utilizados os $(nproc) núcleos/threads do processador. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       make -j$(nproc) 2>&1 | tee -a ${MYLOGPATH}/008-CLHEP-build.log

printf "\n\nGerando testes do CLHEP v2.4.1.3. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       make test 2>&1 | tee -a ${MYLOGPATH}/009-CLHEP-tests-build.log

printf "\n\nInstalando os testes do CLHEP v2.4.1.3. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       sudo make install  2>&1 | tee -a ${MYLOGPATH}/010-CLHEP-tests-install.log

# Como o shell não está rodando em modo interativo,
# é necessário exportar as mesmas variáveis a serem
# utilizadas no decorrer da instalação. Ao final do
# script será dada uma informação ao usuário
# indicando executar o comando source ~/.bashrc

printf "\n\nAjustando as variáveis do sistema:\n\n" | tee -a ${MYLOG}

export CLHEP_DIR=${sGATE}/CLHEP/CLHEP-install
export CLHEP_INCLUDE_DIR=${CLHEP_DIR}/include/
export CLHEP_LIBRARY=${CLHEP_DIR}/lib/

if [ -z "${LD_LIBRARY_PATH}" ]
then
     export LD_LIBRARY_PATH=${CLHEP_LIBRARY}
else
     export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CLHEP_LIBRARY}
fi

export PATH=${PATH}:${CLHEP_DIR}/bin/

# Colocando as informações no LOG de instalação

printf "As linhas a seguir serão adicionadas ao arquivo ${MYBRC}:\n"      | tee -a ${MYLOG}

printf "\n##############################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n# Variáveis para o CLHEP v2.4.1.3"                              | tee -a ${MYLOG} ${MYBRC}
printf "\n##############################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\nexport CLHEP_DIR=${CLHEP_DIR}"                                  | tee -a ${MYLOG} ${MYBRC}
printf "\nexport CLHEP_INCLUDE_DIR=\${CLHEP_DIR}/include/"                | tee -a ${MYLOG} ${MYBRC}
printf "\nexport CLHEP_LIBRARY=\${CLHEP_DIR}/lib/"                        | tee -a ${MYLOG} ${MYBRC}
if [ -z "${LD_LIBRARY_PATH}" ]
then
    printf "\nexport LD_LIBRARY_PATH=\${CLHEP_LIBRARY}"                     | tee -a ${MYLOG} ${MYBRC}
else
    printf "\nexport LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:\${CLHEP_LIBRARY}" | tee -a ${MYLOG} ${MYBRC}
fi
printf "\nexport PATH=\$PATH:\${CLHEP_DIR}/bin/"                          | tee -a ${MYLOG} ${MYBRC}


printf "\n\nIniciando a instalação do Geant4 v10.06.p02:\n"  | tee -a ${MYLOG}

printf "\nCriando o diretório para o Geant4 v10.06.p02:\n"   | tee -a ${MYLOG}
       cd ${sGATE}
       mkdir ${sGATE}/geant4
       cd ${sGATE}/geant4

printf "\nCriando os diretórios para a compilação e a instalação do Geant4 v10.06.p02:" | tee -a ${MYLOG}
       mkdir ${sGATE}/geant4/geant4-build
       mkdir ${sGATE}/geant4/geant4-install

printf "\n\nBaixando o Geant4 v10.06.p02:\n\n" | tee -a ${MYLOG}
       wget http://cern.ch/geant4-data/releases/geant4.10.06.p02.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nDescompactando o Geant4 v10.06.p02:\n\n" | tee -a ${MYLOG}
       tar -zxvf geant4.10.06.p02.tar.gz 2>&1 | tee -a ${MYLOG}


printf "\n\nIniciando a configuração para a compilação do Geant4 v10.06.p02. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}/geant4/geant4-build
       cmake -DCMAKE_INSTALL_PREFIX=${sGATE}/geant4/geant4-install \
       -DGEANT4_INSTALL_DATA=ON \
       -DGEANT4_USE_GDML=ON \
       -DGEANT4_USE_QT=ON \
       -DGEANT4_USE_OPENGL_X11=ON \
       -DGEANT4_USE_SYSTEM_EXPAT=OFF \
       -DGEANT4_USE_SYSTEM_CLHEP=ON \
       -DCLHEP_ROOT_DIR=${CLHEP_DIR} \
       ${sGATE}/geant4/geant4.10.06.p02 2>&1 | tee -a ${MYLOGPATH}/011-Geant4-config.log


printf "\n\nIniciando a compilação do Geant4 v10.06.p02, serão utilizados os $(nproc) núcleos/threads do processador. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       make -j$(nproc) 2>&1 | tee -a ${MYLOGPATH}/012-Geant4-build.log

printf "\n\nInstalando o Geant4 v10.06.p02. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       make install 2>&1 | tee -a ${MYLOGPATH}/013-Geant4-install.log

# Como o shell não está rodando em modo interativo,
# é necessário exportar as mesmas variáveis a serem
# utilizadas no decorrer da instalação. Ao final do
# script será dada uma informação ao usuário
# indicando executar o comando source ~/.bashrc

if [ -f ${sGATE}/geant4/geant4-install/bin/geant4.sh ]; then
   export GEANT4_DIR=${sGATE}/geant4/geant4-install/bin/geant4.sh
   . ${sGATE}/geant4/geant4-install/bin/geant4.sh
fi
export Geant4_DIR=${GEANT4_DIR}

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"  | tee -a ${MYLOG}
printf "\n\n###############################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n# Execução do script de inicialização do Geant4 v10.06.p02"                        | tee -a ${MYLOG} ${MYBRC}
printf "\n###############################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nif [ -f ${sGATE}/geant4/geant4-install/bin/geant4.sh ]; then"                      | tee -a ${MYLOG} ${MYBRC}
printf "\n . ${sGATE}/geant4/geant4-install/bin/geant4.sh"                                   | tee -a ${MYLOG} ${MYBRC}
printf "\nfi" | tee -a ${MYLOG} ${MYBRC}

printf "\n\n---------------------------------------------------" | tee -a ${MYLOG}
printf "\n--                                                 --" | tee -a ${MYLOG}
printf "\n-- Finalizada a instalação do Geant4 v10.06.p02.  --"  | tee -a ${MYLOG}
printf "\n--                                                --"  | tee -a ${MYLOG}
printf "\n----------------------------------------------------"  | tee -a ${MYLOG}

#******************************************************
# Gerando teste apenas para verificar que o Geant 4
# Está operacional
#******************************************************

printf "\n\nCriando diretório de teste para o Geant4 v10.06.p02:\n" | tee -a ${MYLOG}
       mkdir ${sGATE}/geant4-projects

printf "\nCopiando o diretório B1 de ${sGATE}/geant4/geant4.10.06.p02/examples/ para ${sGATE}/geant4-projects"
       cp -r ${sGATE}/geant4/geant4-install/share/Geant4-10.6.2/examples/basic/B1 ${sGATE}/geant4-projects

#########################################
# Observação apenas:
# -Talvez seja interessante no futuro
# ajustar a variável G$WORKDIR no
# .bashrc. Por enquanto isso não
# está sendo feito
##########################################

       export G4WORKDIR=${sGATE}/geant4-projects/B1/
       cd ${G4WORKDIR}
       mkdir ${G4WORKDIR}/B1-build
       cd ${G4WORKDIR}/B1-build

printf "\n\nIniciando a configuração para a compilação do teste B1 para o Geant4 v10.06.p02. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       cmake ..  2>&1 | tee -a ${MYLOGPATH}/014-teste-B1-Geant4-config.log

printf "\n\nIniciando a compilação do teste B1 para o Geant4 v10.06.p02, serão utilizados os $(nproc) núcleos/threads do processador. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       make -j$(nproc)  2>&1 | tee -a ${MYLOGPATH}/015-teste-B1-Geant4-build.log

printf "\n\nO exemplo de teste foi gerado com sucesso em ${G4WORKDIR}/B1-build." | tee -a ${MYLOG}

################################################
# Teste para o Geant4, talvez possa ser ativado
################################################

#while true; do
#  read -r -p "Deseja executar o programa de teste B1 para o Geant4 v10.06.p02 (S|N)?  " input
#  case ${input} in
#       [Ss])
#            printf "\n\nDeseja executar o programa de teste (S|N)?\n\n" | tee -a ${MYLOG}
#            #exec ./exampleB1 & echo "PID do exampleB1 = $!" #./exampleB1;
#            #gnome-terminal -e ./exampleB1  #gnome-terminal -e command
#            #ps aux | grep gnome-terminal
#            #break;;
#        [Nn])
#            echo "\nVocê respondeu: $input.\n" | tee -a $MYLOG
#            echo "\nVocê optou por não executar o programa de teste." | tee -a $MYLOG
#            echo "\nProsseguindo com a instalação do Gate v9.0" | tee -a $MYLOG;
#            break;;
#        *)
#            echo "\nPor favor responda S para sim ou N para não.\n" | tee -a $MYLOG;;
   #esac
#done

printf "\n\n---------------------------------------------------" | tee -a ${MYLOG}
printf "\n--                                                 --" | tee -a ${MYLOG}
printf "\n-- Iniciando a instalação do Gate v9.0:            --" | tee -a ${MYLOG}
printf "\n--                                                 --" | tee -a ${MYLOG}
printf "\n---------------------------------------------------" | tee -a ${MYLOG}

printf "\n\nIniciando a instalação de outras dependências do Gate v9.0: " | tee -a ${MYLOG}

printf "\n\nInstalando o python3 e o pip3:\n\n" | tee -a ${MYLOG}
       sudo apt-get install -y python3 python3-pip | tee -a ${MYLOG}

printf "\n\nAdicionando um alias para o python3 e o pip3." | tee -a ${MYLOG}

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"  | tee -a ${MYLOG}
printf "\n\n##############################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n# Alias para o python3 e o pip3."                                 | tee -a ${MYLOG} ${MYBRC}
printf "\n##############################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nalias python=python3"                                             | tee -a ${MYLOG} ${MYBRC}
printf "\nalias pip=pip3" | tee -a ${MYLOG} ${MYBRC}

printf "\n\nInstalando numpy, matplotlib, scipy, pydicom, pandas, uprootm, SimpleITK, Jupyter Notebook, JupyterLab e Voila:\n\n" | tee -a ${MYLOG}
       pip3 install numpy matplotlib scipy pydicom pandas uproot SimpleITK notebook jupyterlab voila | tee -a ${MYLOG}

# Adicionando a pasta $HOME/.local/bin ao PATH
printf "\n\nAdicionando ${HOME}/.local/bin ao PATH. Conforme indicação dos avisos fornecidos por alguns dos pacotes instalados anteriormente pelo pip3:" | tee -a ${MYLOG}
export PATH="${PATH}:${HOME}/.local/bin"

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"  | tee -a ${MYLOG}

printf "\n\n################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n# Adicionando ${HOME}/.local/bin ao PATH, conforme indicação dos"   | tee -a ${MYLOG} ${MYBRC}
printf "\n# avisos fornecidos por alguns dos pacotes instalados pelo pip3."   | tee -a ${MYLOG} ${MYBRC}
printf "\n##################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\nexport PATH=\$PATH:\$HOME/.local/bin"                               | tee -a ${MYLOG} ${MYBRC}

printf "\n\nInstalando outras dependências:\n\n" | tee -a ${MYLOG}
       sudo apt-get -y install dpkg-dev cmake binutils libx11-dev \
            libxpm-dev libxft-dev libxext-dev libcurl4-openssl-dev gfortran \
            libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev \
            libmysqlclient-dev libfftw3-dev libcfitsio-dev graphviz-dev \
            libavahi-compat-libdnssd-dev libldap2-dev python-dev libxml2-dev \
             libkrb5-dev libgsl0-dev  | tee -a ${MYLOG}

printf "\n\nIniciando a instalação do Root:" | tee -a ${MYLOG}


printf "\n\nBaixando o Root v6.22.00:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}
       wget https://root.cern/download/root_v6.22.00.Linux-ubuntu19-x86_64-gcc9.2.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nDescompactando o Root v6.22.00:\n\n" | tee -a ${MYLOG}
       tar -zxvf root_v6.22.00.Linux-ubuntu19-x86_64-gcc9.2.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nAjustando as variáveis do sistema:" | tee -a ${MYLOG} 
source ${sGATE}/root/bin/thisroot.sh

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:" | tee -a ${MYLOG}

printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n# Execução do script de inicialização do root:"                                      | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nsource ${sGATE}/root/bin/thisroot.sh"                                                | tee -a ${MYLOG} ${MYBRC}

printf "\n\nInstalando o git e git-lfs:\n\n" | tee -a ${MYLOG}
       sudo apt-get -y install git git-lfs | tee -a ${MYLOG}

printf "\n\nInicializando git-lfs:\n\n" | tee -a ${MYLOG}
      git lfs install | tee -a ${MYLOG}

printf "\n\nInstalando algumas dependências para o VTK v7.1.0:\n\n" | tee -a ${MYLOG}
       sudo apt-get install -y libavcodec-dev libavformat-dev libavutil-dev \
libboost-dev libdouble-conversion-dev libeigen3-dev libexpat1-dev \
libfontconfig-dev libfreetype6-dev libgdal-dev libglew-dev libhdf5-dev \
libjpeg-dev libjsoncpp-dev liblz4-dev liblzma-dev libnetcdf-dev \
libnetcdf-cxx-legacy-dev libogg-dev libpng-dev libpython3-dev \
libqt5opengl5-dev libqt5x11extras5-dev libsqlite3-dev libswscale-dev \
libtheora-dev libtiff-dev libxml2-dev libxt-dev qtbase5-dev \
qttools5-dev zlib1g-dev | tee -a ${MYLOG}


printf "\n\nIniciando a instalação do VTK v7.1.1:" | tee -a ${MYLOG}

printf "\n\nClonando repositório o VTK de https://github.com/Kitware/VTK.git:\n\n" | tee -a ${MYLOG}
       cd  ${sGATE}
       git  clone --verbose --progress https://github.com/Kitware/VTK.git 2>&1 | tee -a ${MYLOG}

printf "\n\nAjustando VTK para a versão v7.1.1:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}/VTK
       git checkout v7.1.1 # foi a que conseguir fazer funcionar

echo   "\n\nBaixando alguns dados complementares para o VTK v7.1.1:\n\n" | tee -a ${MYLOG}
       wget https://www.vtk.org/files/release/7.1/VTKData-7.1.1.zip 2>&1 | tee -a ${MYLOG}
       unzip '*.zip' | tee -a ${MYLOG}
       mv ${sGATE}/VTK/VTK-7.1.1 ${sGATE}/VTK/vtk-data

# O diretório vtk-data é um ojeto que se porta como um conjunto de diretórios ocultos
# ele será ajustado para o CMAKE capturar os dados para gerar os testes. Ainda assim
# será necessário ter uma boa rede, pois outros dados serão baixados. Ajustei as variáveis
# de time_out para valores mais altos:
# -DCTEST_TEST_TIMEOUT=10000    -> o normal seria 3600
# -DCTEST_SUBMIT_RETRY_COUNT=10 -> 3
# -DCTEST_SUBMIT_RETRY_DELAY=10 -> 5
# -DDART_TESTING_TIMEOUT=10000  -> 3600
# Entretanto estou usando todos os
# threads disponíveis do processador

printf "\n\nCriando o diretório de instalação para o VTK v7.1.1:" | tee -a ${MYLOG}
       mkdir ${sGATE}/VTK/vtk-build
       cd ${sGATE}/VTK/vtk-build

printf "\n\nIniciando a configuração para a compilação do VTK v7.1.1. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
      cmake -DBUILD_DOCUMENTATION=OFF \
            -DBUILD_EXAMPLES=OFF  \
            -DBUILD_SHARED_LIBS=ON  \
            -DBUILD_TESTING=ON  \
            -DCMAKE_BACKWARDS_COMPATIBILITY=2.4 \
            -DCMAKE_BUILD_TYPE=Release \
            -DCTEST_TEST_TIMEOUT=10000 \
            -DCTEST_SUBMIT_RETRY_COUNT=10  \
            -DCTEST_SUBMIT_RETRY_DELAY=10  \
            -DDART_TESTING_TIMEOUT=10000 \
            -DVTK_ANDROID_BUILD=OFF \
            -DVTK_EXTRA_COMPILER_WARNINGS=OFF \
            -DVTK_GLEXT_FILE=${sGATE}/VTK/Utilities/ParseOGLExt/headers/glext.h \
            -DVTK_GLXEXT_FILE=${sGATE}/VTK/Utilities/ParseOGLExt/headers/glxext.h \
            -DVTK_Group_Imaging=OFF \
            -DVTK_Group_MPI=OFF \
            -DVTK_Group_Qt=OFF \
            -DVTK_Group_Rendering=ON \
            -DVTK_Group_StandAlone=ON \
            -DVTK_Group_Tk=OFF \
            -DVTK_Group_Views=OFF \
            -DVTK_Group_Web=OFF \
            -DVTK_IOS_BUILD=OFF \
            -DVTK_PYTHON_VERSION=3.8 \
            -DVTK_RENDERING_BACKEND=OpenGL2 \
            -DVTK_SMP_IMPLEMENTATION_TYPE=Sequential \
            -DVTK_USE_CXX11_FEATURES=OFF \
            -DVTK_USE_LARGE_DATA=OFF \
            -DVTK_WGLEXT_FILE=${sGATE}/VTK/Utilities/ParseOGLExt/headers/wglext.h \
            -DVTK_WRAP_JAVA=OFF \
            -DVTK_WRAP_PYTHON=OFF \
            -DVTK_WRAP_TCL=OFF \
            -DModule_vtkDICOM=ON \
            -DModule_vtkGUISupportQt=ON \
            -DModule_vtkGUISupportQtOpenGL=ON \
            -DModule_vtkImagingOpenGL2=ON \
            -DModule_vtkRenderingQt=ON \
            -DPYTHON_EXECUTABLE=/usr/bin/python3.8 \
            -DVTK_QT_VERSION=5  \
            ${sGATE}/VTK 2>&1 | tee -a ${MYLOGPATH}/016-VTK-config.log

# Não está sendo usado no momento
#-DVTK_DATA_STORE=${sGATE}/VTK/vtk-data \

printf "\n\nIniciando a compilação do VTK v7.1.1, serão utilizados os $(nproc) núcleos/threads do processador. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       make -j$(nproc) 2>&1 | tee -a ${MYLOGPATH}/017-VTK-build.log

# Como o shell não está rodando em modo interativo,
# é necessário exportar as mesmas variáveis a serem
# utilizadas no decorrer da instalação. Ao final do
# script será dada uma informação ao usuário
# indicando executar o comando source ~/.bashrc

printf "\n\nAjustando as variáveis do sistema:"  | tee -a ${MYLOG}
export VTK_DIR=${sGATE}/VTK/vtk-build/bin

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}

printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n# Variável para o VTK v7.1.1"                                                        | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nexport VTK_DIR=${VTK_DIR}"                                                           | tee -a ${MYLOG} ${MYBRC}

printf "\n\nIniciando a instalação do ITK e o RTK v5.1.0:" | tee -a ${MYLOG}
     cd ${sGATE}

printf "\n\nBaixando o ITK v5.1.0:\n\n" | tee -a ${MYLOG}
       wget https://github.com/InsightSoftwareConsortium/ITK/releases/download/v5.1.0/InsightToolkit-5.1.0.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nDescompactando o ITK v5.1.0:\n\n" | tee -a ${MYLOG}
        tar -zxvf InsightToolkit-5.1.0.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nRenomeando InsightToolkit-5.1.0 como itk-5.1.0:" | tee -a ${MYLOG}
       mv ${sGATE}/InsightToolkit-5.1.0 ${sGATE}/itk-5.1.0
       cd ${sGATE}/itk-5.1.0

printf "\n\nCriando o diretório para compilação do ITK e RTK v5.1.0:"  | tee -a ${MYLOG}
       mkdir itk-build itk-install
       cd itk-build

printf "\n\nIniciando a configuração para a compilação e instalação do ITK v5.1.0. com a opção RTK=ON. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       cmake -DCMAKE_INSTALL_PREFIX=${sGATE}/itk-5.1.0/itk-install \
             -DBUILD_EXAMPLES=OFF \
             -DBUILD_TESTING=OFF \
             -DITKV3_COMPATIBILITY=OFF \
             -DITK_BUILD_DEFAULT_MODULES=ON \
             -DITK_WRAP_PYTHON=OFF \
             -DBUILD_SHARED_LIBS=ON \
             -DModule_ITKVtkGlue=ON \
             -DModule_RTK=ON \
             -DVTK_DIR=${sGATE}/VTK/vtk-build \
             -DRTK_USE_CUDA=OFF \
             -DCUDA_64_BIT_DEVICE_CODE=OFF \
             -DCUDA_ATTACH_VS_BUILD_RULE_TO_C=OFF \
             -DCUDA_BUILD_CUBIN=OFF \
             -DCUDA_BUILD_EMULATION=OFF \
             -DCUDA_BUILD_EMULATION=OFF \
             -DCUDA_FOUND=OFF \
             -DCUDA_HAVE_GPU=OFF \
             -DCUDA_HOST_COMPILATION_CPP=OFF \
             -DCUDA_PROPAGATE_HOST_FLAGS=OFF \
             -DCUDA_SEPARABLE_COMPILATION=OFF \
             -DCUDA_USE_STATIC_CUDA_RUNTIME=OFF \
             -DITK_USE_GPU=OFF \
             -DITK_USE_SYSTEM_EXPAT=ON \
             ${sGATE}/itk-5.1.0 2>&1 | tee -a ${MYLOGPATH}/018-ITK-config.log

printf "\n\nIniciando a compilação e instalação do ITK v5.1.0. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"
       sudo make install 2>&1 | tee -a ${MYLOGPATH}/019-ITK-build.log

# Como o shell não está rodando em modo interativo,
# é necessário exportar as mesmas variáveis a serem
# utilizadas no decorrer da instalação. Ao final do
# script será dada uma informação ao usuário
# indicando executar o comando source ~/.bashrc

printf "\n\nAjustando as variáveis do sistema." | tee -a ${MYLOG}
if [ -z "${LD_LIBRARY_PATH}" ]
then
    export LD_LIBRARY_PATH=${sGATE}/itk-5.1.0/itk-build/lib
else
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${sGATE}/itk-5.1.0/itk-build/lib
fi
PATH=${PATH}:${sGATE}/itk-5.1.0/itk-build/bin 

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}

printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Variáveis para os ITK e RTK v5.1.0"                                                 | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
if [ -z "$LD_LIBRARY_PATH" ]
then
    printf "\nexport LD_LIBRARY_PATH=${sGATE}/itk-5.1.0/itk-build/lib" | tee -a ${MYLOG} ${MYBRC}
else
    printf "\nexport LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:${sGATE}/itk-5.1.0/itk-build/lib" | tee -a ${MYLOG} ${MYBRC}
fi
printf "\nexport PATH=\$PATH:${sGATE}/itk-5.1.0/itk-build/bin" | tee -a ${MYLOG} ${MYBRC}


printf "\n\nInstalando o libtorch v1.4.0+cpu:" | tee -a ${MYLOG}

printf "\n\nFazendo o download dos arquivos de suporte:\n\n" | tee -a ${MYLOG}
      cd ${sGATE}
      wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1cDQpGSkKAr6QR_fcVBi6bU3P8qE64Opl' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1cDQpGSkKAr6QR_fcVBi6bU3P8qE64Opl" -O support.zip && rm -rf /tmp/cookies.txt
                                                                                                                                                                                                                                                                                                         

printf "\n\nDescompactando o arquivo support.zip e o libtorch v1.4.0-cpu em ${sGATE}:\n\n" | tee -a ${MYLOG}
       unzip ${sGATE}/support.zip | tee ${MYLOG}
       printf "\n\n"
       unzip ${sGATE}/libtorch-cxx11-abi-shared-with-deps-1.4.0+cpu.zip | tee ${MYLOG}

printf "\n\nAjustando as variáveis do sistema:"  | tee -a ${MYLOG}
export LIBTORCH_DIR=${sGATE}/libtorch
export LIBTORCH_INCLUDE_DIR=${LIBTORCH_DIR}/include/
export LIBTORCH_LIBRARY=${LIBTORCH_DIR}/lib/
if [ -z "${LD_LIBRARY_PATH}" ]
then
    export LD_LIBRARY_PATH=${LIBTORCH_LIBRARY}
else
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${LIBTORCH_LIBRARY}
fi
export PATH=${PATH}:${LIBTORCH_DIR}/bin/

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}

printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Variáveis para o libtorch v1.4+cpu"                                                 | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nexport LIBTORCH_DIR=${LIBTORCH_DIR}"                                                 | tee -a ${MYLOG} ${MYBRC}
printf "\nexport LIBTORCH_INCLUDE_DIR=\${LIBTORCH_DIR}/include/"                               | tee -a ${MYLOG} ${MYBRC}
printf "\nexport LIBTORCH_LIBRARY=\${LIBTORCH_DIR}/lib/"                                       | tee -a ${MYLOG} ${MYBRC}
if [ -z "${LD_LIBRARY_PATH}" ]
then
    printf "\nexport LD_LIBRARY_PATH=\${LIBTORCH_LIBRARY}"                     | tee -a ${MYLOG} ${MYBRC}
else
    printf "\nexport LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:\${LIBTORCH_LIBRARY}" | tee -a ${MYLOG} ${MYBRC}
fi
printf "\nexport PATH=\$PATH:\${LIBTORCH_DIR}/bin/"                            | tee -a ${MYLOG} ${MYBRC}

printf "\n\nIniciando a instalação do Gate v9.0:"                              | tee -a ${MYLOG}
       cd ${sGATE}

printf "\n\nBaixando o Gate v9.0 de https://github.com/OpenGATE/Gate/archive/v9.0.zip\n\n" | tee -a ${MYLOG}
       wget https://github.com/OpenGATE/Gate/archive/v9.0.zip 2>&1 | tee -a ${MYLOG} 

printf "\n\nDescompactando o Gate v9.0:\n\n" | tee -a ${MYLOG}
       unzip v9.0.zip | tee ${MYLOG}

printf "\n\nCriando os diretórios para a compilação e instalação do Gate v9.0:"  | tee -a ${MYLOG}
      cd ${sGATE}/Gate-9.0
      mkdir ${sGATE}/Gate-9.0/gate-build ${sGATE}/Gate-9.0/gate-install

printf "\n\nIniciando a configuração para a compilacao do Gate v9.0. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       cd ${sGATE}/Gate-9.0/gate-build

      cmake -DCMAKE_INSTALL_PREFIX=${sGATE}/Gate-9.0/gate-install \
            -DBUILD_TESTING=ON \
            -DC10_LIBRARY=${sGATE}/libtorch/lib/libc10.so \
            -DCLHEP_DIR=~${sGATE}/CLHEP/CLHEP-install/lib/CLHEP-2.4.1.3 \
            -DCLHEP_ROOT_DIR=${sGATE}/CLHEP/CLHEP-install \
            -DCMAKE_BACKWARDS_COMPATIBILITY=2.4 \
            -DCMAKE_BUILD_TYPE=Release \
            -DCaffe2_DIR=${sGATE}/libtorch/share/cmake/Caffe2 \
            -DGATE_COMPILE_WITH_NATIVE=OFF \
            -DGATE_DOWNLOAD_BENCHMARKS_DATA=ON \
            -DGATE_USE_DAVIS=ON \
            -DGATE_USE_ECAT7=OFF \
            -DGATE_USE_GEANT4_UIVIS=ON \
            -DGATE_USE_ITK=ON \
            -DGATE_USE_LMF=OFF \
            -DGATE_USE_OPTICAL=ON \
            -DGATE_USE_RTK=ON \
            -DGATE_USE_SYSTEM_CLHEP=ON \
            -DGATE_USE_TORCH=ON \
            -DGATE_USE_XRAYLIB=OFF \
            -DGeant4_DIR=${sGATE}/geant4/geant4-install/lib/Geant4-10.6.2 \
            -DITK_DIR=${sGATE}/itk-5.1.0/itk-build \
            -DQt5Core_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5Core \
            -DQt5Gui_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5Gui \
            -DQt5OpenGL_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5OpenGL \
            -DQt5PrintSupport_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5PrintSupport \
            -DQt5Widgets_DIR=/usr/lib/x86_64-linux-gnu/cmake/Qt5Widgets \
            -DROOTCINT_EXECUTABLE=${sGATE}/root/bin/rootcint \
            -DTORCH_LIBRARY=${sGATE}/libtorch/lib/libtorch.so \
            -DTorch_DIR=${sGATE}/libtorch/share/cmake/Torch \
            ${sGATE}/Gate-9.0 2>&1 | tee -a ${MYLOGPATH}/020-Gate-config.log

printf "\n\nIniciando a compilação do Gate v9.0, serão utilizados os $(nproc) núcleos/threads do processador. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n"  | tee -a ${MYLOG}
       make -j$(nproc)  2>&1 | tee -a ${MYLOGPATH}/021-Gate-build.log

printf "\n\nInstalando o Gate v9.0. Um log dessa operação será gerado em ${MYLOGPATH}:\n\n" | tee -a ${MYLOG}
       make install 2>&1 | tee -a ${MYLOGPATH}/022-Gate-install.log

printf "\n\nAjustando as variáveis do sistema." | tee -a ${MYLOG}
export PATH=${PATH}:${sGATE}/Gate-9.0/gate-install/bin

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}

printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Variável para o Gate V.9.0"                                                         | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nexport PATH=\$PATH:${sGATE}/Gate-9.0/gate-install/bin"                               | tee -a ${MYLOG} ${MYBRC}

printf "\n\nAdicionando o suporte para paralelização:" | tee -a ${MYLOG}

printf "\n\nAdicionando o jobsplitter:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}/Gate-9.0/cluster_tools/jobsplitter
       make 2>&1 | tee -a ${MYLOGPATH}/023-Gate-jobsplitter-config.log
       sudo cp ${sGATE}/Gate-9.0/cluster_tools/jobsplitter/gjs ${sGATE}/Gate-9.0/gate-install/bin

printf "\n\nAdicionando o filemerger:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}/Gate-9.0/cluster_tools/filemerger
       make 2>&1 | tee -a ${MYLOGPATH}/024-Gate-filemerger-config.log
       sudo cp ${sGATE}/Gate-9.0/cluster_tools/filemerger/gjm ${sGATE}/Gate-9.0/gate-install/bin

printf "\n\n---------------------------------------------------"   | tee -a ${MYLOG}
printf "\n--                                                 --"   | tee -a ${MYLOG}
printf "\n-- Finalizada a instalação do Gate v9.0.           --"   | tee -a ${MYLOG}
printf "\n--                                                 --"   | tee -a ${MYLOG}
printf "\n---------------------------------------------------\n\n" | tee -a ${MYLOG}

printf "\n\nInstalação de outros softwares de apoio:" | tee -a ${MYLOG}

printf "\n\nCriando exemplos em ${HOME}/GateContrib:" | tee -a ${MYLOG}
       cd ${sGATE}

printf "\n\nClonando o repositório GateContrib de https://github.com/OpenGATE/GateContrib.git:\n\n" | tee -a ${MYLOG}
       git clone --verbose --progress https://github.com/OpenGATE/GateContrib.git 2>&1 | tee -a ${MYLOG}

printf "\n\nCriando exercícios em ${sGATE}/gate-exercices:" | tee -a ${MYLOG}
     cd ${sGATE}

printf "\n\nClonando o repositório gate-exercices de https://github.com/dsarrut/gate-exercices.git:\n\n" | tee -a ${MYLOG}
       git clone --verbose --progress https://github.com/dsarrut/gate-exercices.git 2>&1 | tee -a ${MYLOG}

printf "\n\nInstalando GateTools, utilizado o pip3:\n\n" | tee -a ${MYLOG}
       pip3 install gatetools | tee -a ${MYLOG}

printf "\n\nAjustando as variáveis do sistema:" | tee -a ${MYLOG}
export PATH=${PATH}:${HOME}/.local/bin

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}

printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Variável para o GateTools"                                                          | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nexport PATH=\$PATH:\$HOME/.local/bin"                                                | tee -a ${MYLOG} ${MYBRC}

printf "\n\nExecutando os testes do GateTools:\n\n" | tee -a ${MYLOG}
printf python3 -m unittest gatetools -v             | tee -a ${MYLOG}

printf "\n\n*************************************\n\n" | tee -a ${MYLOG}
       python3 -m unittest gatetools.phsp -v           | tee -a ${MYLOG}
printf "\n\n*************************************"     | tee -a ${MYLOG}

printf "\n\nInformações de como utilizar o GateTools podem ser obtidas em https://github.com/OpenGATE/GateTools.\n\n" | tee -a ${MYLOG}

printf "\n\nIniciando a instalação do vV v1.4:"    | tee -a ${MYLOG}

printf "\n\nFazendo o Download dos binários do vV v1.4:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}
#     # Necessário utilizar o curl por conta de erro 403 ao tentar usar
#     # o comando wget com a url da página

       curl 'https://www.creatis.insa-lyon.fr/rio/vv?action=AttachFile&do=get&target=vv-1.4Qt4-linux64.tar.gz' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Connection: keep-alive' -H 'Referer: https://www.creatis.insa-lyon.fr/rio/vv?action=AttachFile&do=view&target=vv-1.4Qt4-linux64.tar.gz' -H 'Upgrade-Insecure-Requests: 1' --output ${sGATE}/vv-1.4Qt4-linux64.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nInstalando os binários do vV v1.4:\n\n" | tee -a ${MYLOG}
      mkdir vv-1.4Qt4
      tar -zxvf ${sGATE}/vv-1.4Qt4-linux64.tar.gz --directory ${sGATE}/vv-1.4Qt4 2>&1 | tee -a ${MYLOG}

printf "\n\nAjustando as variáveis do sistema:"                                                | tee -a ${MYLOG}

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:\n\n"                     | tee -a ${MYLOG}
printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Alias para o vv-1.4Q-t4"                                                            | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nalias vv=${sGATE}/vv-1.4Qt4/vv" | tee -a ${MYLOG} ${MYBRC}

printf "\n\nIniciando a instalação do 3D Slicer v4.10.2:"  | tee -a ${MYLOG}

printf "\n\nInstalando algumas dependências para o 3D Slicer v4.10.2:\n\n" | tee -a ${MYLOG}
#       sudo apt-get install libpulse-dev libnss3 libglu1-mesa | tee -a ${MYLOG}
      sudo apt-get install -y libpulse-dev libnss3 libglu1-mesa | tee -a ${MYLOG}

printf "\n\nInstalando o 3D Slicer v4.10.2:\n\n" | tee -a ${MYLOG}
       cd ${sGATE}
       wget https://download.slicer.org/bitstream/1023242 2>&1 | tee -a ${MYLOG}
       cp 1023242 Slicer-4.10.2-linux-amd64.tar.gz
       rm 1023242

printf "\n\nDescompactando o 3D Slicer v4.10.2:\n\n" | tee -a ${MYLOG}
       tar -zxvf $sGATE/Slicer-4.10.2-linux-amd64.tar.gz 2>&1 | tee -a ${MYLOG}

printf "\n\nAjustando as variáveis do sistema:"                                                | tee -a ${MYLOG}

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}
printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Alias para o Slicer v4.10.2"                                                        | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nexport PATH=\$PATH:${sGATE}/Slicer-4.10.2-linux-amd64"                               | tee -a ${MYLOG} ${MYBRC}
printf "\nalias slicer=${sGATE}/Slicer-4.10.2-linux-amd64/Slicer"                              | tee -a ${MYLOG} ${MYBRC}

printf "\n\nInstalando o Fiji-ImageJ com alguns plugins de interesse em Medicina Nuclear:/n/n" | tee -a ${MYLOG}
       cd ${sGATE}
       unzip ${sGATE}/Fiji.app.zip

printf "\n\nAjustando as variáveis do sistema:"                                                | tee -a ${MYLOG}

printf "\n\nAs linhas a seguir serão adicionadas ao arquivo ${MYBRC}:"                         | tee -a ${MYLOG}
printf "\n\n#################################################################################" | tee -a ${MYLOG} ${MYBRC}
printf "\n#Variável e alias para o Fiji - ImageJ"                                              | tee -a ${MYLOG} ${MYBRC}
printf "\n#################################################################################"   | tee -a ${MYLOG} ${MYBRC}
printf "\nexport PATH=\$PATH:${sGATE}/Fiji.app:"                                               | tee -a ${MYLOG} ${MYBRC}
printf "\nalias fiji=${sGATE}/Fiji.app/ImageJ-linux64"                                         | tee -a ${MYLOG} ${MYBRC}

while true; do
  printf "\n\n"
  read -n 1 -r -p "Deseja Instalar o Visual Studio Code (S|N)? " input
  printf "\nDeseja Instalar o Visual Studio Code (S|N)? ${input}\n" >> ${MYLOG}
  case ${input} in
       [Ss])
            printf "\n\nContinuando com a instalação do Visual Studio Code:" | tee -a ${MYLOG}
            break;;
       [Nn])
            printf "\n\nVocê optou por não seguir com o processo de instalação do Visual Studio Code." | tee -a ${MYLOG}
            cd ${sGATE}

            printf "\n\nRemovendo arquivos de suporte de instalação *.zip e *.tar.gz." | tee -a ${MYLOG}
            files_zip=$( ls *.zip 2>/dev/null | wc -l)
            if [ ${files_zip} != 0  ]
            then
                rm -v *.zip | tee -a ${MYLOG}
            else
                printf "\n\nNão existem arquivos *.zip para remover." | tee -a ${MYLOG}
            fi

            files_tar_gz=$( ls *.tar.gz 2>/dev/null | wc -l)
            if [ ${files_tar_gz} != 0  ]
            then
                rm -v *.tar.gz | tee -a ${MYLOG}
            else
                printf "\n\nNão existem arquivos *.tar.gz para remover."  | tee -a ${MYLOG}
            fi
            
            files_log=$( ls *.log 2>/dev/null | wc -l)
            if [ ${files_log} != 0  ]
            then
                mv *.log ${MYLOGPATH}  | tee -a ${MYLOG}
            fi

            echo
            read -n 1 -s -r -p "Ao rodar o programa Gate v9.0, se você obtiver uma mensagem de erro relacionada ao RDRAND, você poderá consultar a https://arstechnica.com/gadgets/2019/10/how-a-months-old-amd-microcode-bug-destroyed-my- weekend/ e ler sobre esse problema (Aperte  qualquer tecla para sair." input

           printf "\n\nAo rodar o programa Gate v9.0, se voce obtiver uma mensagem de erro relacionada ao RDRAND, você poderá consultar a https://arstechnica.com/gadgets/2019/10/how-a-months-old-amd-microcode-bug-destroyed-my- weekend/ e ler sobre esse problema." >> ${MYLOG}
           printf "\n\nTecle qualquer tecla para sair.\n ${input}" >> ${MYLOG}
           
           printf "\n\n---------------------------------------------------" | tee -a ${MYLOG}
           printf "\n--                                                 --" | tee -a ${MYLOG}
           printf "\n-- Finalizada a instalação do Gate v9.0 e dos      --" | tee -a ${MYLOG}
           printf "\n-- softwares de suporte ao curso.                  --" | tee -a ${MYLOG}
           printf "\n--                                                 --" | tee -a ${MYLOG}
           printf "\n---------------------------------------------------" | tee -a ${MYLOG}

           printf "\n\nEncerrando o log de instalação do Gate v9.0.\n\n" | tee -a ${MYLOG}

          #Encerrando o processo de instalação
          exit 0 ;;
      *)
          printf "\n\nPor favor responda S para sim ou N para não.\n\n" | tee -a ${MYLOG};;
  esac
done

printf "\n\nInstalando o Visual Studio Code:\n\n" | tee -a ${MYLOG}
        curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg  2>&1 | tee -a ${MYLOG}
        sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/  2>&1 | tee -a ${MYLOG}

printf "" | tee -a ${MYLOG}
        sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] \
https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'  2>&1 | tee -a ${MYLOG}
#        sudo apt-get install apt-transport-https | tee -a ${MYLOG}

printf "" | tee -a ${MYLOG}
       sudo apt-get install -y apt-transport-https

printf "" | tee -a ${MYLOG}
       sudo apt-get update | tee -a ${MYLOG}

printf "" | tee -a ${MYLOG}
        sudo apt-get install code | tee -a ${MYLOG}
#       sudo apt-get install -y code | tee -a {MYLOG}

printf  "\n\nInstalação do Visual Studio Code concluída." | tee -a ${MYLOG}

         cd ${sGATE}

         printf "\n\nRemovendo arquivos de suporte de instalação *.zip e *.tar.gz." | tee -a ${MYLOG}

         files_zip=$( ls *.zip 2>/dev/null | wc -l)
         if [ ${files_zip} != 0  ]
         then
             rm -v *.zip | tee -a ${MYLOG}
         else
             printf "\n\nNão existem arquivos *.zip para remover."  | tee -a ${MYLOG}
         fi

        files_tar_gz=$( ls *.tar.gz 2>/dev/null | wc -l)
        if [ ${files_tar_gz} != 0  ]
        then
           rm -v *.tar.gz | tee -a ${MYLOG}
        else
           printf "\n\nNão existem arquivos *.tar.gz para remover." | tee -a ${MYLOG}
        fi

        files_log=$( ls *.log 2>/dev/null | wc -l)
        if [ ${files_log} != 0  ]
        then
            mv *.log ${MYLOGPATH}  | tee -a ${MYLOG}
        fi

        printf "\n"
        read -n 1 -s -r -p "Ao rodar o programa Gate v9.0, se você obtiver uma mensagem de erro relacionada ao RDRAND,  você poderá consultar a https://arstechnica.com/gadgets/2019/10/how-a-months-old-amd-microcode-bug-destroyed-my- weekend/ e ler sobre esse problema (Aperte  qualquer tecla para sair." input

        printf "\n\nAo rodar o programa Gate v9.0, se você obtiver uma mensagem de erro relacionada ao RDRAND,  você poderá consultar a https://arstechnica.com/gadgets/2019/10/how-a-months-old-amd-microcode-bug-destroyed-my- weekend/ e ler sobre esse problema." >> ${MYLOG}
        printf "\n\nTecle qualquer tecla para sair: ${input}\n" >> ${MYLOG}
        printf "\n\n---------------------------------------------------" | tee -a ${MYLOG}
        printf "\n--                                                 --" | tee -a ${MYLOG}
        printf "\n-- Finalizada a instalação do Gate v9.0 e dos      --" | tee -a ${MYLOG}
        printf "\n-- softwares de suporte ao curso.                  --" | tee -a ${MYLOG}
        printf "\n--                                                 --" | tee -a ${MYLOG}
        printf "\n---------------------------------------------------"   | tee -a ${MYLOG}

        printf "\n\nEncerrando o log de instalação do Gate v9.0.\n\n" | tee -a ${MYLOG}


#Encerrando o processo de instalação
exit 0

