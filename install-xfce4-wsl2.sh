#!/bin/bash

# Script destinado à instalação do ambiente gráfico XFCE4
# em sistema rodando o Ubuntu 20.04 LTS


# Atualizando o sistema
sudo apt-get update && sudo apt-get -y upgrade

# Instalando o servidor xrdp (Remote Desktop Protocol}
sudo apt-get install -y xrdp

# Instalando o ambiente gráfico XFCE4
sudo apt-get install -y xfce4 gdm3

# Instalando aplicações do XFCE4
sudo apt-get install -y xfce4-goodies

# Fazendo o backup do arquivo xrdp.ini
sudo cp /etc/xrdp/xrdp.ini /etc/xrdp/xrdp.ini.bak

# Alterando o arquivo xrdp.ini para a configuração 
# da porta desejada (3389 -> 3390)
sudo sed -i 's/3389/3390/g' /etc/xrdp/xrdp.ini

# Alterando o arquivo xrdp.ini para as configurações
# de vídeo desejadas
sudo sed -i 's/max_bpp=32/#max_bpp=32\nmax_bpp=128/g' /etc/xrdp/xrdp.ini
sudo sed -i 's/xserverbpp=24/#xserverbpp=24\nxserverbpp=128/g' /etc/xrdp/xrdp.ini

# Exportando a sessão do XFCE4 para o arquivo ~/.xsession
echo xfce4-session>~/.xsession

# Fazendo o backup do arquivo startwm.sh
sudo cp /etc/xrdp/startwm.sh /etc/xrdp/startwm.sh.bak

# Alterando o arquivos start.sh para suportar a inicialização
# do ambiente XFCE4
sudo sed -i '33 s/test/#test/' /etc/xrdp/startwm.sh
sudo sed -i '34 s/exec/#exec/' /etc/xrdp/startwm.sh
sudo sed -i '$astartxfce4' /etc/xrdp/startwm.sh 

# Inicializando o servidor xrdp
sudo /etc/init.d/xrdp start

# Adcionando ao arquivo ~/.bashrc
printf "\n#====================================" | tee -a ~/.bashrc
printf "\n# Inicializando o servidor xrdp"       | tee -a ~/.bashrc
printf "\n#====================================" | tee -a ~/.bashrc
printf "\nsudo /etc/init.d/xrdp start"          | tee -a ~/.bashrc 

printf "\n\n"
